export class ReactiveFormControl {
  constructor(public value: any, private validators: Function[] = []) {}

  public errors: string[] = [];
  public touched = false;

  public reset() {
    this.value = null;
    this.resetErrors();
    this.setTouchStatus(false);
  }

  public validate() {
    this.errors = [];
    if (!this.validators || !this.validators.length) return;
    this.validators.forEach((validator: Function) => {
      const error = validator.call(this, this.value);
      if (error !== null) {
        if (!this.errors) this.errors = [];
        this.errors.push(error);
      }
    });
  }

  public setTouchStatus(status: boolean) {
    this.touched = status
  }

  public resetErrors() {
    this.errors = [];
  }
}
