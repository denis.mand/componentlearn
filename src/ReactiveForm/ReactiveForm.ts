import { ReactiveFormControl } from "@/ReactiveForm/ReactiveFormControl";
export interface FormControl {
  [key: string]: ReactiveFormControl;
}

export class ReactiveForm {
  public controls: FormControl | null = null;
  public value: any;

  public get hasErrors(): number {
    if (!this.controls) return 0;

    let acc = 0;
    for (const key of Object.keys(this.controls)) {
      this.controls[key].errors !== null
        ? (acc += this.controls[key].errors.length)
        : acc;
    }
    return acc;
  }

  public createForm(controls: FormControl) {
    this.controls = controls;
  }

  public submit() {
    this.validate();
    if (this.hasErrors) return;
    this.getValue();
    console.log(this.value);
  }

  private validate() {
    if (!this.controls) return;

    for (const key of Object.keys(this.controls)) {
      this.controls[key].validate();
    }
  }

  private getValue(): void {
    if (!this.controls) return;
    this.value = null;

    const value: { [key: string]: any } = {};

    for (const key of Object.keys(this.controls)) {
      value[key] = this.controls[key].value;
    }
    this.value = value;
  }
}
